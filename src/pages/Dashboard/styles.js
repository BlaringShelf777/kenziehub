import styled from "styled-components"

export const Container = styled.div`
	width: 100%;
	height: 100vh;
	display: flex;
`

export const Info = styled.div`
	width: 30%;
	padding: 1rem;

	& > div {
		padding: 1rem;
		display: flex;
		justify-content: space-between;
		align-items: center;

		nav {
			margin-right: 1rem;
		}
	}

	h3 {
		margin-top: 8rem;
	}

	p {
		color: white;
		font-weight: lighter;
		padding: 1rem;
		max-width: 400px;
		line-height: 1.5;
	}
`

export const Options = styled.div`
	width: 70%;
	background-color: white;
	padding: 2rem;

	h1 {
		text-transform: uppercase;
		font-weight: bold;
		color: var(--orange);
		margin-bottom: 2rem;
	}
`
export const Form = styled.form`
	display: flex;
	gap: 1rem;
	max-width: 600px;
	width: 100%;
	justify-content: center;
	align-items: center;
	margin: 0 auto;

	input {
		padding: 1rem;
	}

	div div {
		width: 100%;
	}

	button {
		width: 50px;
		height: 50px;
		border: 0;
		padding: 0;
		border-radius: 50%;
		display: flex;
		justify-content: center;
		align-items: center;
	}
`

export const Error = styled.p`
	font-size: 0.8rem;
	color: red;
	text-align: left;
`
