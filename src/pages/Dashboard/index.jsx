import { FormControl, MenuItem, Select, TextField } from "@material-ui/core"
import { useForm } from "react-hook-form"
import { Redirect, useHistory } from "react-router-dom"
import Button from "../../components/Button"
import Callouts from "../../components/Callouts"
import Logo from "../../components/Logo"
import Tech from "../../components/Tech"
import { Container, Error, Form, Info, Options } from "./styles"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import api from "../../services/api"
import { toast } from "react-toastify"
import { useState } from "react"
import { useEffect } from "react"

export default function Dashboard({ authorization, setAuthenticated, user }) {
	const history = useHistory()
	const [token] = useState(
		JSON.parse(localStorage.getItem("@kenziehub:token")) || ""
	)
	const [techs, setTechs] = useState([])
	const [selection, setSelection] = useState("")

	const schema = yup.object().shape({
		title: yup.string().required("Required field"),
		status: yup.string().required("Required field"),
	})

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({ resolver: yupResolver(schema) })

	const handleNavigation = (path) => history.push(path)

	const getTechs = (user_id = user?.id) => {
		if (user_id)
			return api
				.get(`users/${user_id}`)
				.then(({ data: { techs } }) => setTechs(techs))
				.catch((err) => console.log(err))
		return setTechs([])
	}

	const logout = () => {
		localStorage.clear()
		setAuthenticated(false)
		handleNavigation("/")
	}

	const onSubmit = (tech) => {
		api
			.post("/users/techs", tech, {
				headers: {
					Authorization: `Bearer ${token}`,
				},
			})
			.then(
				({
					data: {
						user: { id },
					},
				}) => getTechs(id)
			)
			.catch((err) => console.log(err))
	}

	const remove = (tech_id) => {
		api
			.delete(`/users/techs/${tech_id}`, {
				headers: {
					Authorization: `Bearer ${token}`,
				},
			})
			.then((_) => getTechs())
			.catch((err) => console.log(err))
	}

	useEffect(() => {
		getTechs()
	}, [])

	if (!authorization) return <Redirect to="/" />

	return (
		<Container>
			<Info>
				<div>
					<Logo />
				</div>
				<Callouts>
					Hello, <span>{user.name}</span>
				</Callouts>
				<div>
					<Button onClick={logout}>
						<em>exit</em>
					</Button>
				</div>
			</Info>
			<Options>
				<h1>Add new techs</h1>
				<Form onSubmit={handleSubmit(onSubmit)}>
					<div>
						<TextField
							{...register("title")}
							label="Tech name"
							variant="outlined"
							error={!!errors.title?.message}
						/>
						<Error>{errors.title?.message}</Error>
					</div>
					<div>
						<FormControl variant="outlined">
							<Select
								{...register("status")}
								labelId="select_label"
								error={!!errors.status?.message}
								value={selection}
								onChange={(evt) => setSelection(evt.target.value)}
							>
								<MenuItem value="">
									<em>None</em>
								</MenuItem>
								<MenuItem value="Iniciante">Beginner</MenuItem>
								<MenuItem value="Intermediário">Intermediate</MenuItem>
								<MenuItem value="Avançado">Advanced</MenuItem>
							</Select>
						</FormControl>
						<Error>{errors.status?.message}</Error>
					</div>

					<Button type="submit">+</Button>
				</Form>
				{console.log(techs)}
				<div>
					{techs.map(({ title, status, id }) => (
						<Tech
							key={id}
							title={title}
							status={status}
							id={id}
							remove={remove}
						/>
					))}
				</div>
			</Options>
		</Container>
	)
}
