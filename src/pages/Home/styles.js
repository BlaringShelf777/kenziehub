import styled from "styled-components"

export const Container = styled.div`
	width: 100%;
	height: 100vh;
	display: flex;
`

export const Info = styled.div`
	width: 60%;
	padding: 1rem;

	& > div {
		padding: 1rem;
		display: flex;
		justify-content: space-between;
		align-items: center;

		nav {
			margin-right: 1rem;
		}
	}

	h3 {
		margin-top: 10rem;
	}

	p {
		color: white;
		font-weight: lighter;
		padding: 1rem;
		max-width: 400px;
		line-height: 1.5;
	}
`

export const Options = styled.div`
	width: 40%;
	background-color: white;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	gap: 2rem;
`
