import { Redirect, useHistory } from "react-router-dom"
import Button from "../../components/Button"
import Callouts from "../../components/Callouts"
import Logo from "../../components/Logo"
import Navigation from "../../components/Navigation"
import { Container, Info, Options } from "./styles"

export default function Home({ authorization }) {
	const history = useHistory()

	const handleNavigation = (path) => history.push(path)

	if (authorization) return <Redirect to="/dashboard" />

	return (
		<Container>
			<Info>
				<div>
					<Logo />
					<Navigation />
				</div>
				<Callouts>
					Keep track of your <span>KNOWLEDGE</span>
				</Callouts>
				<p>
					documentate your progress in as many programming languages as you
					wish!
				</p>
			</Info>
			<Options>
				<Button onClick={() => handleNavigation("/signup")}>
					Sign up today
				</Button>
				<Button secondary onClick={() => handleNavigation("/login")}>
					Login now
				</Button>
			</Options>
		</Container>
	)
}
