import styled from "styled-components"

export const Container = styled.div`
	display: grid;
	place-items: center;
	width: 100%;
	height: 100vh;

	div {
		text-align: center;
		h1 {
			font-weight: lighter;
			font-size: 11rem;
			color: var(--orange);
		}

		p {
			text-transform: uppercase;
			font-weight: bold;
			color: white;
			font-size: 2rem;
		}
	}
`
