import { Container } from "./styles"

export default function Page404() {
	return (
		<Container>
			<div>
				<h1>404</h1>
				<p>page not found</p>
			</div>
		</Container>
	)
}
