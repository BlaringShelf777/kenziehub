import styled from "styled-components"

export const Container = styled.div`
	width: 100%;
	height: 100vh;
	display: flex;
`

export const Info = styled.div`
	width: 30%;
	padding: 1rem;

	& > div {
		padding: 1rem;
		display: flex;
		justify-content: space-between;
		align-items: center;

		nav {
			margin-right: 1rem;
		}
	}

	h3 {
		margin-top: 8rem;
	}

	p {
		color: white;
		font-weight: lighter;
		padding: 1rem;
		max-width: 400px;
		line-height: 1.5;
	}
`

export const Options = styled.div`
	width: 70%;
	background-color: white;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	gap: 2rem;

	h1 {
		text-transform: uppercase;
		font-weight: bold;
		color: var(--orange);
	}
`
export const Form = styled.form`
	display: flex;
	flex-direction: column;
	gap: 1rem;
	max-width: 400px;

	input,
	textarea {
		padding: 1rem;
	}

	textarea {
		max-height: 350px;
	}

	div.password {
		display: flex;
		gap: 1rem;
	}

	div:not(.password) div {
		width: 100%;
	}

	button {
		width: 100%;
		border: 0;
		height: min-content;
		padding: 0.5rem;
	}

	p {
		font-size: 0.8rem;
		text-align: right;

		a {
			font-weight: bold;
			color: var(--orange);
		}
	}
`

export const Error = styled.p`
	font-size: 0.8rem;
	color: red;
	text-align: left;
`
