import { TextField } from "@material-ui/core"
import { useForm } from "react-hook-form"
import { Link, Redirect, useHistory } from "react-router-dom"
import Button from "../../components/Button"
import Callouts from "../../components/Callouts"
import Logo from "../../components/Logo"
import { Container, Error, Form, Info, Options } from "./styles"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import api from "../../services/api"
import { toast } from "react-toastify"

export default function Signup({ authorization }) {
	const history = useHistory()

	const schema = yup.object().shape({
		name: yup.string().required("Required field"),
		email: yup.string().required("Required field").email("Invalid email"),
		password: yup.string().required("Required field").min(6),
		_password: yup
			.string()
			.required("Required field")
			.oneOf([yup.ref("password")], "Passwords are different"),
		bio: yup.string().required("Required field"),
		contact: yup.string().required("Required field"),
		module: yup
			.string()
			.required("Required field")
			.test("Valid module", "Module not found", (val) =>
				[
					"Primeiro módulo (Introdução ao Frontend)",
					"Segundo módulo (Frontend Avançado)",
					"Terceiro módulo (Introdução ao Backend)",
					"Quarto módulo (Backend Avançado)",
				].includes(val)
			),
	})

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({ resolver: yupResolver(schema) })

	const handleNavigation = (path) => history.push(path)

	const onSubmit = ({ name, email, password, bio, contact, module }) => {
		const new_user = {
			email,
			password,
			name,
			bio,
			contact,
			course_module: module,
		}

		api
			.post("/users", new_user)
			.then((_) => {
				handleNavigation("/login")
			})
			.catch((err) => {
				console.log(err)
				return toast.error("Couldn't sign up :(")
			})
	}

	if (authorization) return <Redirect to="/dashboard" />

	return (
		<Container>
			<Info>
				<div>
					<Logo />
				</div>
				<Callouts>
					join the <span>crew</span>
				</Callouts>
				<p>create your profile and be part of this amazing community!</p>
			</Info>
			<Options>
				<h1>Sign Up</h1>
				<Form onSubmit={handleSubmit(onSubmit)}>
					<div>
						<TextField
							{...register("name")}
							label="Name"
							variant="outlined"
							error={!!errors.name?.message}
						/>
						<Error>{errors.name?.message}</Error>
					</div>
					<div>
						<TextField
							{...register("email")}
							label="Email"
							variant="outlined"
							error={!!errors.email?.message}
						/>
						<Error>{errors.email?.message}</Error>
					</div>
					<div className="password">
						<div>
							<TextField
								{...register("password")}
								label="Password"
								variant="outlined"
								error={!!errors.password?.message}
							/>
							<Error>{errors.password?.message}</Error>
						</div>
						<div>
							<TextField
								{...register("_password")}
								label="Confirm password"
								variant="outlined"
								error={!!errors._password?.message}
							/>
							<Error>{errors._password?.message}</Error>
						</div>
					</div>
					<div>
						<TextField
							{...register("bio")}
							label="Bio"
							variant="outlined"
							error={!!errors.bio?.message}
						/>
						<Error>{errors.bio?.message}</Error>
					</div>
					<div>
						<TextField
							{...register("contact")}
							label="Contact"
							variant="outlined"
							error={!!errors.contact?.message}
						/>
						<Error>{errors.contact?.message}</Error>
					</div>
					<div>
						<TextField
							{...register("module")}
							label="Course Module"
							variant="outlined"
							error={!!errors.module?.message}
						/>
						<Error>{errors.module?.message}</Error>
					</div>
					<Button type="submit">Let's go</Button>
					<p>
						Already have an account? <Link to="/login">Login</Link> time!
					</p>
				</Form>
			</Options>
		</Container>
	)
}
