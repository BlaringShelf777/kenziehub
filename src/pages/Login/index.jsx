import { TextField } from "@material-ui/core"
import { useForm } from "react-hook-form"
import { Link, Redirect, useHistory } from "react-router-dom"
import Button from "../../components/Button"
import Callouts from "../../components/Callouts"
import Logo from "../../components/Logo"
import { Container, Error, Form, Info, Options } from "./styles"
import * as yup from "yup"
import { yupResolver } from "@hookform/resolvers/yup"
import api from "../../services/api"
import { toast } from "react-toastify"

export default function Login({ authorization, setAuthorization }) {
	const history = useHistory()

	const schema = yup.object().shape({
		email: yup.string().required("Required field").email("Invalid email"),
		password: yup.string().required("Required field").min(6),
	})

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({ resolver: yupResolver(schema) })

	const handleNavigation = (path) => history.push(path)

	const onSubmit = (user) => {
		api
			.post("/sessions", user)
			.then((resp) => {
				localStorage.setItem(
					"@kenziehub:token",
					JSON.stringify(resp.data.token)
				)
				localStorage.setItem("@kenziehub:user", JSON.stringify(resp.data.user))
				setAuthorization(true)
				handleNavigation("/dashboard")
			})
			.catch((err) => {
				console.log(err)
				return toast.error("Incorrect email or password!")
			})
	}

	if (authorization) return <Redirect to="/dashboard" />

	return (
		<Container>
			<Info>
				<div>
					<Logo />
				</div>
				<Callouts>
					wellcome <span>sailor</span>
				</Callouts>
				<p>Time to update your skills!</p>
			</Info>
			<Options>
				<h1>Login</h1>
				<Form onSubmit={handleSubmit(onSubmit)}>
					<div>
						<TextField
							{...register("email")}
							label="Email"
							variant="outlined"
							error={!!errors.email?.message}
						/>
						<Error>{errors.email?.message}</Error>
					</div>
					<div>
						<TextField
							{...register("password")}
							label="Password"
							variant="outlined"
							error={!!errors.password?.message}
						/>
						<Error>{errors.password?.message}</Error>
					</div>

					<Button type="submit">Let's go!</Button>
					<p>
						Don't have an account? <Link to="/signup">Sign Up</Link> time!
					</p>
				</Form>
			</Options>
		</Container>
	)
}
