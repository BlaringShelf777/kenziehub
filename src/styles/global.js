import { createGlobalStyle } from "styled-components"
import bg from "../assets/blurbg.jpg"

export default createGlobalStyle`
    :root {
        --orange: #F07E3D;
        --gray: #E2E2E2;
        --black: #202020;
    }

    /* reset */
    * {
        padding: 0;
        margin: 0;
        outline: 0;
        box-sizing: border-box;
        font-family: 'Karla', sans-serif;
    }

    body {
        background: url(${bg}) no-repeat;
        background-size: 100vw;
        background-position-x: left;
        background-position-y: bottom;
        min-height: 100vh;
    }

    h2 {
        font-size: 60px;
        color: white;
        font-weight: bold;
    }
`
