import { Container } from "./styles"

export default function Button({ children, secondary = false, ...rest }) {
	return (
		<Container secondary={secondary} {...rest}>
			{children}
		</Container>
	)
}
