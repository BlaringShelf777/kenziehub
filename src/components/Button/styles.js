import styled, { css } from "styled-components"

export const Container = styled.button`
	padding: 1rem;
	width: 250px;
	height: 60px;
	border: 3px solid var(--orange);
	background-color: var(--orange);
	text-align: center;
	border-radius: 10px;
	color: white;
	cursor: pointer;
	font-size: 1.2rem;

	${(props) =>
		props.secondary &&
		css`
			background-color: white;
			color: var(--orange);
		`};
`
