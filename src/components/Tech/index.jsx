import { Container } from "./styles"

export default function Tech({ title, status, id, remove }) {
	return (
		<Container>
			<h4>{title}</h4>
			<p>{status}</p>
			<button onClick={() => remove(id)}>-</button>
		</Container>
	)
}
