import styled from "styled-components"

export const Container = styled.div`
	display: flex;
	width: 100%;
	margin: 0 auto;
	padding: 1rem;
	max-width: 600px;
	align-items: center;
	gap: 2rem;

	h4 {
		font-size: 1rem;
		font-weight: bold;
	}

	p {
		font-size: 0.8rem;
		font-weight: lighter;
	}

	button {
		width: 20px;
		height: 20px;
		border-radius: 50%;
		border: 0;
		background-color: var(--orange);
		color: white;
		cursor: pointer;
	}
`
