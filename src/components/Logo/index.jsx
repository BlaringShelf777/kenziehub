import { useHistory } from "react-router-dom"
import { Container } from "./styles"

export default function Logo({ secondary }) {
	const history = useHistory()

	return (
		<Container onClick={() => history.push("/")}>
			{secondary ? (
				<h2>
					KENZIE<span>HUB</span>
				</h2>
			) : (
				<h1>
					KENZIE<span>HUB</span>
				</h1>
			)}
		</Container>
	)
}
