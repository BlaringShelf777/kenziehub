import styled from "styled-components"

export const Container = styled.div`
	cursor: pointer;

	h1,
	h2 {
		font-size: 48px;
		font-weight: normal;
		color: white;
		span {
			color: var(--orange);
		}
	}
`
