import { Container } from "./styles"

export default function Callouts({ children }) {
	return <Container>{children}</Container>
}
