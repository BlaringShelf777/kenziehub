import styled from "styled-components"

export const Container = styled.h3`
	color: white;
	font-weight: bold;
	font-size: 3.5rem;
	text-transform: uppercase;
	padding: 1rem;
	line-height: 3.5rem;

	span {
		color: var(--orange);
		font-size: 4.5rem;
	}
`
