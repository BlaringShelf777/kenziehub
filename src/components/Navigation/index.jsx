import { motion } from "framer-motion"
import { Nav } from "./styles"

const li = {
	visible: {
		opacity: 1,
		x: 0,
	},
	hidden: {
		opacity: 0,
		x: 20,
	},
}

const ul = {
	visible: {
		opacity: 1,
		transition: {
			when: "beforeChildren",
			staggerChildren: 0.3,
		},
	},
	hidden: {
		opacity: 0,
		transition: {
			when: "afterChildren",
		},
	},
}

export default function Navigation() {
	return (
		<Nav>
			<motion.ul initial="hidden" animate="visible" variants={ul}>
				<motion.li variants={li}>About</motion.li>
				<motion.li variants={li}>Contact Us</motion.li>
				<motion.li variants={li}>Report an Issue</motion.li>
			</motion.ul>
		</Nav>
	)
}
