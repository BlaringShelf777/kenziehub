import styled from "styled-components"

export const Nav = styled.nav`
	ul {
		list-style: none;
		display: flex;
		gap: 2rem;

		li {
			color: white;
			font-weight: lighter;
			cursor: pointer;
		}
	}
`
