import Routes from "./Routes"
import GlobalStyle from "./styles/global"
import "react-toastify/dist/ReactToastify.min.css"
import { ToastContainer } from "react-toastify"

function App() {
	return (
		<>
			<GlobalStyle />
			<Routes />
			<ToastContainer
				position="top-right"
				autoClose={5000}
				hideProgressBar={false}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss
				draggable
				pauseOnHover
			/>
		</>
	)
}

export default App
