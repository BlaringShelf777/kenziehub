import { useEffect } from "react"
import { useState } from "react"
import { Route, Switch } from "react-router-dom"
import Dashboard from "../pages/Dashboard"
import Home from "../pages/Home"
import Login from "../pages/Login"
import Page404 from "../pages/Page404"
import Signup from "../pages/Signup"

export default function Routes() {
	const [authenticated, setAuthenticated] = useState(false)
	const [user, setUser] = useState(null)

	useEffect(() => {
		const token = localStorage.getItem("@kenziehub:token")
		const user = JSON.parse(localStorage.getItem("@kenziehub:user"))

		if (token) setAuthenticated(true)
		if (user) setUser(user)
	}, [authenticated])

	return (
		<Switch>
			<Route exact path="/">
				<Home authorization={authenticated} />
			</Route>
			<Route path="/login">
				<Login
					authorization={authenticated}
					setAuthorization={setAuthenticated}
				/>
			</Route>
			<Route path="/signup">
				<Signup authorization={authenticated} />
			</Route>
			<Route path="/dashboard">
				<Dashboard
					authorization={authenticated}
					setAuthenticated={setAuthenticated}
					user={user}
				/>
			</Route>
			<Route>
				<Page404 />
			</Route>
		</Switch>
	)
}
